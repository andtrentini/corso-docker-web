function getOwners() {
    var client = new XMLHttpRequest();

    client.onreadystatechange = () => {
        if (client.readyState == 4 && client.status == 200) {
            var owners = JSON.parse(client.responseText);
            var viewdata = document.getElementById('viewdata');
            viewdata.innerHTML = '';
            viewdata.appendChild(ownerTable(owners));
        }
    }

    client.open('GET', 'http://localhost:3000/owners', true);
    client.send();
}

function ownerRow(owner) {
    var row = document.createElement('tr');
    var cell = document.createElement('td');
    cell.innerText = owner.id;
    row.appendChild(cell);
    cell = document.createElement('td');
    cell.innerText = owner.first_name;
    row.appendChild(cell);
    cell = document.createElement('td');
    cell.innerText = owner.last_name;
    row.appendChild(cell);
    cell = document.createElement('td');
    cell.innerText = owner.email;
    row.appendChild(cell);
    cell = document.createElement('td');
    cell.innerText = owner.gender;
    row.appendChild(cell);
    cell = document.createElement('td');
    var button = document.createElement('button');
    button.innerText = 'Get cars';
    button.classList = 'btn btn-primary';
    button.onclick = () => {
        getOwnerCars(owner.id);
    }
    cell.appendChild(button);
    row.appendChild(cell);
    return row;
}

function ownerTable(owners) {
    var table = document.createElement('table');
    table.className = 'table';
    var thead = document. createElement('thead');
    var header = document.createElement('tr');
    header.innerHTML = '<th>id</th><th>First name</th><th>Last name</th><th>Mail</th><th>Gender</th>';
    thead.appendChild(header);
    table.appendChild(thead);
    var tbody = document.createElement('tbody');
    owners.forEach(owner => {
        tbody.appendChild(ownerRow(owner));
    });    
    table.appendChild(tbody);
    return table;
}

